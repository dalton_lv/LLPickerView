# LLPickerView 选择器
如果需要swift版的，可以issues一下。

## 演示
### 图一
![图一](http://git.oschina.net/uploads/images/2016/1110/171304_fb1cff31_414734.jpeg "")
### 图二
![图二](http://git.oschina.net/uploads/images/2016/1110/171314_583881ed_414734.jpeg "")
### 图三
![图三](http://git.oschina.net/uploads/images/2016/1110/171323_e3f12cfb_414734.jpeg "")

## 多种样式可以控制，风格可以修改
### 使用示例

点击弹出(变换标题)

    LLPickerView *pickerView = [[LLPickerView alloc] initWithData:@[@"疾风剑豪哇塞卡疾风剑豪哇塞卡",@"剑魔剑魔剑魔剑魔剑魔剑魔"] title:@"选择"    leftTitle:@"新建" rightTitle:@"确认"];
    // 控制标题是否为当前选择数据
    pickerView.isAllow = YES;
    // 确认的回调
    pickerView.confirmBlock = ^(NSInteger row, id object){
        NSLog(@"%ld--%@",row,object);
    };
    // 取消的回调，不需要写特殊操作可以忽略
    pickerView.cancelBlock = ^{
        NSLog(@"新建");
    };
    [self.view addSubview:pickerView];
